import asyncio
from datetime import datetime

import httpx

import config
from code.utils import flatten_list


class CalendarificClient:
    _api_key: str
    _base_url: str

    def __init__(self, api_key: str, base_url: str = config.CALENDARIFIC_BASE_URL):
        self._api_key = api_key
        self._base_url = base_url

    async def _get_request(self, url: str, **request_params) -> dict:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, params=request_params)
        return response.json()

    def _get_date_params_for_holydays(self, start_date: datetime, end_date: datetime) -> list[dict]:
        """
        The idea of this function is to calculate list of optimal Calendarific date params (year, month, day).
        By optimal params I mean such params that allow us to make minimum API calls while minimizing the necessary
        filtering in Python. Examples:
        - If start & end date differ only by days - we don't want to make API call for whole year and then filter out
            99% of values.
        - On the other hand, if start & end date differ in years - we don't want to make API call for each month or day.

        In particular numbers:
        If start & end date are in different years:
            if there are whole years between start & end date - the best scenario is to:
                1. for years inbetween - query full years;
                2. for start year - depends on date:
                    a. if start year includes <= 3 months - query by month;
                    b. else - query by year;
                3. for end year - same as start year;
            else (differance is 2 years):
                1. for start year - depends on date:
                    a. if start year includes <= 3 months - query by month;
                    b. else - query by year;
                2. for end year - same as start year;
        Elif start & end date are in the same year:
            if number of months between start & end date > 6:
                1. query whole year;
            else:
                1. query by months;
        Elif start & end date are in the same month:
            1. query by month;
        Else (start date == end date):
            1. query by day;

        """
        if start_date == end_date:
            return [{'year': start_date.year, 'month': start_date.month, 'day': start_date.day}]

        if start_date.year != end_date.year:
            biggest_differ = 'year'
        else:
            if start_date.month != end_date.month:
                biggest_differ = 'month'
            else:
                biggest_differ = 'day'

        if biggest_differ == 'year':
            start_params = []
            if start_date.month > 9:
                for month in range(start_date.month, 13):
                    start_params.append({'year': start_date.year, 'month': month})
            else:
                start_params.append({'year': start_date.year})

            end_params = []
            if end_date.month < 4:
                for month in range(1, end_date.month + 1):
                    end_params.append({'year': end_date.year, 'month': month})
            else:
                end_params.append({'year': end_date.year})

            inbetween_params = []
            years = list(range(start_date.year, end_date.year + 1))
            if len(years) > 2:
                for year in years[1:-1]:
                    inbetween_params.append({'year': year})
            date_params = [*start_params, *inbetween_params, *end_params]
        elif biggest_differ == 'month':
            months = list(range(start_date.month, end_date.month + 1))
            if len(months) > 6:
                date_params = [{'year': start_date.year}]
            else:
                date_params = [{'year': start_date.year, 'month': m} for m in months]
        else:
            date_params = [{'year': start_date.year, 'month': start_date.month}]

        return date_params

    @staticmethod
    def _filter_out_holidays_by_daterange(holidays: list[dict], start_date: datetime, end_date: datetime) -> list[dict]:
        return [
            holiday for holiday in holidays
            if start_date <= datetime.fromisoformat(holiday['date']['iso']).replace(tzinfo=None) <= end_date
        ]

    def _filter_and_merge_holiday_results(
        self, holidays_from_api: list[list[dict]], start_date: datetime, end_date: datetime
    ) -> list[dict]:
        results_len = len(holidays_from_api)
        if results_len == 1:
            return self._filter_out_holidays_by_daterange(holidays_from_api[0], start_date, end_date)
        elif results_len == 2:
            return self._filter_out_holidays_by_daterange(flatten_list(holidays_from_api), start_date, end_date)
        elif results_len > 2:
            return (
                self._filter_out_holidays_by_daterange(holidays_from_api[0], start_date, end_date) +
                flatten_list(holidays_from_api[1:-1]) +
                self._filter_out_holidays_by_daterange(holidays_from_api[-1], start_date, end_date)
            )
        else:
            return []

    async def get_holidays(
        self, countries: list[str], start_date: datetime, end_date: datetime
    ) -> dict[str, list]:
        url = f'{self._base_url}/holidays'
        tasks_per_country = {}
        date_params = self._get_date_params_for_holydays(start_date, end_date)
        for country in countries:
            tasks_per_country[country] = []
            for date_param in date_params:
                tasks_per_country[country].append(
                    self._get_request(url, api_key=self._api_key, country=country, **date_param)
                )

        top_level_tasks = []
        for country in tasks_per_country:
            top_level_tasks.append(asyncio.gather(*tasks_per_country[country]))

        # order of results are same as order of countries in tasks_per_country dict (Thank God dict is ordered nowadays)
        results = await asyncio.gather(*top_level_tasks)
        results_per_country = {}
        for res, country in zip(results, tasks_per_country.keys()):
            result = [data['response']['holidays'] for data in res]
            results_per_country[country] = self._filter_and_merge_holiday_results(result, start_date, end_date)

        return results_per_country
