import itertools
import json
from datetime import datetime
from pathlib import Path


def flatten_list(list_to_flatten: list[list]) -> list:
    return list(itertools.chain.from_iterable(list_to_flatten))


def dump_results(holiday_results: dict[str, list], start_date: datetime, end_date: datetime):
    parent = Path(__file__).resolve().parents[1]
    results_dir = parent / 'holiday_results'
    results_dir.mkdir(exist_ok=True)

    for country_code, holidays in holiday_results.items():
        file = results_dir / f'{country_code.lower()}_{start_date.date()}_{end_date.date()}.json'
        with open(file, 'w') as target:
            json.dump(holidays, target)
