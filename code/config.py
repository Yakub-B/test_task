from decouple import config

CALENDARIFIC_BASE_URL = config('CALENDARIFIC_BASE_URL', default='https://calendarific.com/api/v2')
CALENDARIFIC_API_KEY = config('CALENDARIFIC_API_KEY')
