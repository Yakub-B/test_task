import asyncio
from datetime import datetime

from code.calendarific_client import CalendarificClient
from code import config
from code.utils import dump_results


async def main():
    start_date = datetime(year=1992, month=7, day=7)
    end_date = datetime(year=1992, month=9, day=18)
    countries_list = ['ua', 'us', 'gb']
    client = CalendarificClient(config.CALENDARIFIC_API_KEY)
    results = await client.get_holidays(countries_list, start_date, end_date)
    dump_results(results, start_date, end_date)


if __name__ == '__main__':
    asyncio.run(main())
